import Container from 'react-bootstrap/Container';
import Navbar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import { Link, NavLink, Navigate } from 'react-router-dom';
import { useState, Fragment, useContext, React } from 'react';
import './moriNav.css';
import logo from './images/MoriLogo.png'
import { LinearScale, ShoppingCartOutlined } from '@material-ui/icons'

export default function MoriNav(){
    return(
        <Nav id="navBar" style={{boxShadow:"4px 4px 8px 0px rgba(0,0,0,0.2)"}}>
            <Nav.Link href="/" id="navLink"> </Nav.Link>
            <Nav.Link  id="navLink" href="/">
            {/* <img id="logo" alt="logo" src={logo}/> */}
            <p id="logosub">Mori's<br/>Mementos</p>
            
            </Nav.Link>
            <Nav.Link  href="/products" id="navLink">Products</Nav.Link>
            {
                (localStorage.isAdmin === 'true')?(
                    <Nav.Link  href='/admindashboard' id="navLink">Admin Dashboard</Nav.Link>
                )
                :
                null
            }
            {
                (localStorage.token !== undefined)?(
                    <Nav.Link  href="/logout" id="navLink">Logout</Nav.Link>
                )
                :
                (
                    <Nav.Link  href="/login" id="navLink">Login</Nav.Link>
                )
            }            
            <Nav.Link  href="/register" id="navLink">Register</Nav.Link>
            {
                (localStorage.token!== undefined)?(
                    <Nav.Link  href="/viewCart" id="navLink" ><ShoppingCartOutlined /></Nav.Link>
                )
                :
                (
                    null
                )
            }
            
            <Nav.Link href="/" id="navLink"> </Nav.Link>
            
        </Nav>
    )
}