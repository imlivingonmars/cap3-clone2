import {Row, Col} from 'react-bootstrap';
import {Link } from 'react-router-dom';

export default function Banner({data}){
    const{title, description,destination, label} = data;
    return(
        <Row>
            <Col className="p-5">
                <h1>{title}</h1>
                <div>{description}</div>
                <Link to={destination}>{label}</Link>
            </Col>
        </Row>
    )
}